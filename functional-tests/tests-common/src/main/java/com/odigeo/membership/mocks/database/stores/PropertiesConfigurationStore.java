package com.odigeo.membership.mocks.database.stores;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PropertiesConfigurationStore {

    private static final String INSERT_PROPERTY = "INSERT INTO MEMBERSHIP_OWN.GE_MEMBERSHIP_CONFIG (KEY, VALUE) VALUES (?, ?)";
    private static final String REMOVE_PROPERTY = "DELETE FROM MEMBERSHIP_OWN.GE_MEMBERSHIP_CONFIG WHERE KEY = ?";
    private static final String SELECT_PROPERTY = "SELECT VALUE FROM MEMBERSHIP_OWN.GE_MEMBERSHIP_CONFIG WHERE KEY = ?";

    public void resetPropertyValue(final Connection conn, final String key, final boolean value) throws SQLException {
        removeProperty(conn, key);
        insertProperty(conn, key, value);
    }

    private void insertProperty(final Connection conn, final String key, final boolean value) throws SQLException {
        try (PreparedStatement pstmt = conn.prepareStatement(INSERT_PROPERTY)) {
            pstmt.setString(1, key);
            pstmt.setBoolean(2, value);
            pstmt.execute();
        }
    }

    private void removeProperty(final Connection conn, final String key) throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement(REMOVE_PROPERTY)) {
            ps.setString(1, key);
            ps.executeUpdate();
        }
    }

    public boolean findPropertyValue(final Connection conn, final String key) throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement(SELECT_PROPERTY)) {
            ps.setString(1, key);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getBoolean("VALUE");
                }
            }
        }
        throw new SQLException("No value found for the property configuration with the key " + key);
    }
}
