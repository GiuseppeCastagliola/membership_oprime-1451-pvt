package com.odigeo.membership.functionals.membership.booking;

import com.odigeo.commons.test.random.Picker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class MembershipBookingHelper {

    private MembershipBookingHelper() {
    }

    public static List<MembershipBookingBuilder> createMembershipBookingRandomList(long memberId, int numBookings, int numDays, Picker picker) {
        List<MembershipBookingBuilder> result = new ArrayList<>();

        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);

        Calendar before = Calendar.getInstance();
        before.add(Calendar.DAY_OF_MONTH, -numDays);

        for (int i = 0; i < numBookings; i++) {
            Date bookingDate = Date.from(picker.pickDateBetween(before, today).toInstant());
            MembershipBookingBuilder membershipBookingBuilder = new MembershipBookingBuilder().setMemberId(memberId).setBookingDate(bookingDate).setBookingId(1L);
            result.add(membershipBookingBuilder);
        }

        return result;
    }

}
