package com.odigeo.membership.functionals.membership;

import com.odigeo.membership.request.container.TravellerParametersContainer;

public class TravellerParametersContainerBuilder {

    private String name;
    private String lastNames;

    public TravellerParametersContainerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public TravellerParametersContainerBuilder setLastNames(String lastNames) {
        this.lastNames = lastNames;
        return this;
    }

    public TravellerParametersContainer build() {
        TravellerParametersContainer travellerParametersContainer = new TravellerParametersContainer();
        travellerParametersContainer.setName(this.name);
        travellerParametersContainer.setLastNames(this.lastNames);
        return travellerParametersContainer;
    }


}
