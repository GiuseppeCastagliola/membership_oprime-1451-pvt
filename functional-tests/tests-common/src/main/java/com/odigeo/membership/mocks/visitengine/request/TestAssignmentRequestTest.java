package com.odigeo.membership.mocks.visitengine.request;

public class TestAssignmentRequestTest {

    public String testName;
    public String assignedTestLabel;
    public Integer winnerPartition;
    public boolean enabled;

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getAssignedTestLabel() {
        return assignedTestLabel;
    }

    public void setAssignedTestLabel(String assignedTestLabel) {
        this.assignedTestLabel = assignedTestLabel;
    }

    public Integer getWinnerPartition() {
        return winnerPartition;
    }

    public void setWinnerPartition(Integer winnerPartition) {
        this.winnerPartition = winnerPartition;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
