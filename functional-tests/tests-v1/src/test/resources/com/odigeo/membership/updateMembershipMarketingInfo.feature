Feature: Test the updateMembershipMarketingInfo service

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |

  Scenario Outline: Send subscribed message to marketing from activated member
    Given the user-api has the following user info
      | userId | status              | email   | brand | token   | tokenType   |
      | 4321   | <userAccountStatus> | <email> | ED    | <token> | <tokenType> |
    And the next membership stored in db:
      | memberId       | website | status             | autoRenewal   | memberAccountId | activationDate | expirationDate | balance |
      | <membershipId> | ES      | <membershipStatus> | <autoRenewal> | 123             | 2017-07-06     | 2018-07-06     | 54.99   |
    When requested update member info in marketing for <membershipId> and <email>
    Then the message with member info is sent to kafka for <membershipId> and <email> with status <subscriptionStatus>
    And the message with shouldSetPassword <shouldSetPassword> and passwordToken <token> is sent to kafka
  Examples:
    | membershipId | membershipStatus | autoRenewal | email             | userAccountStatus | token     | tokenType        | shouldSetPassword | subscriptionStatus |
    | 123          | ACTIVATED        | ENABLED     | test1@edreams.com | PENDING_LOGIN     | testToken | REQUEST_PASSWORD | true              | SUBSCRIBED         |
    | 321          | DEACTIVATED      | ENABLED     | test2@edreams.com | PENDING_LOGIN     | null      |                  | false             | UNSUBSCRIBED       |
    | 422          | EXPIRED          | DISABLED    | test3@edreams.com | ACTIVE            | null      |                  | false             | UNSUBSCRIBED       |
