Feature: Test getAllMembership service

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 135             | 1022   | Epifanio  | Martinez  |
      | 140             | 1022   | EPIFANIO  | MARTINEZ  |
      | 166             | 1034   | Carmen    | Fernandez |

    And the next membership stored in db:
      | memberId | website     | status              | autoRenewal | memberAccountId | activationDate | expirationDate | balance | sourceType     | monthsDuration |
      | 103      | ES          | ACTIVATED           | ENABLED     | 135             | 2020-03-31     | 2021-03-31     | 54.99   | FUNNEL_BOOKING | 12             |
      | 106      | ES          | PENDING_TO_ACTIVATE | ENABLED     | 135             | 2020-03-30     | 2021-03-30     | 54.99   | FUNNEL_BOOKING | 12             |
      | 107      | IT          | ACTIVATED           | DISABLED    | 140             | 2020-02-14     | 2021-08-14     | 0       | POST_BOOKING   | 6              |
      | 201      | IT          | EXPIRED             | DISABLED    | 166             | 2019-03-28     | 2020-03-28     | 54.99   | FUNNEL_BOOKING | 12             |


  Scenario Outline: getAllMembership returns memberships that are activated on any website
    When requested getAllMembership with userId <userId>
    Then the resulting WebsiteMembership contains <listSize> memberships
    Examples:
      | userId | listSize |
      | 1022   | 2        |
      | 1034   | 0        |