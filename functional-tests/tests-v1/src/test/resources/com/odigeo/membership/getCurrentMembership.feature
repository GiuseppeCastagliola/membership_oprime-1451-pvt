Feature: Test getMembership service

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
      | 321             | 1234   | ANA       | JUNYENT   |
      | 555             | 1555   | MARIO     | GOMEZ     |

    And the next membership stored in db:
      | memberId | website     | status              | autoRenewal | memberAccountId | activationDate | expirationDate | balance | sourceType     | monthsDuration |
      | 123      | ES          | ACTIVATED           | ENABLED     | 123             | 2017-07-06     | 2018-07-06     | 54.99   | FUNNEL_BOOKING | 12             |
      | 321      | FR          | PENDING_TO_COLLECT  | ENABLED     | 321             | 2017-07-06     | 2018-07-06     | 54.99   | FUNNEL_BOOKING | 12             |
      | 322      | ES          | PENDING_TO_COLLECT  | ENABLED     | 123             | 2016-07-06     | 2017-07-06     | 54.99   | FUNNEL_BOOKING | 12             |
      | 987      | ES          | PENDING_TO_ACTIVATE | ENABLED     | 555             | 2017-11-11     | 2018-11-11     | 54.99   | FUNNEL_BOOKING | 6              |
  Scenario Outline: Retrieving current membership
    When requested the current membership with userId <userId> and website <website>
    Then the retrieved membership has status <status>
    Examples:
      | userId | website | status             |
      | 4321   | ES      | ACTIVATED          |
      | 1234   | FR      | PENDING_TO_COLLECT |
  Scenario: User without current membership
    When requested the current membership with userId 1555 and website ES
    Then there is not current membership