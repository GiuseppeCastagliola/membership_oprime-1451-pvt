Feature: Update userId with updateMemberAccount service

  Scenario Outline: The user wants to update the userId but has no active or PTC memberships
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
    And the next membership stored in db:
      | memberId | website     | status      | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 4451     | ES          | DEACTIVATED | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
    When the user wants to update the MemberAccount:
      | memberAccountId | userId   | operation   |
      | 123             | <userId> | <operation> |
    Then the MemberAccount stored in db has the following information:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
    And the number of membership subscription messages sent is 0
    Examples:
      | userId | operation      |
      | 257    | UPDATE_USER_ID |

  Scenario Outline: The user wants to update the userId and has either an active or PTC membership
    Given the next memberAccount stored in db:
      | memberAccountId | userId          | firstName | lastNames |
      | 123             | <currentUserId> | JOSE      | GARCIA    |
    And the next membership stored in db:
      | memberId | website     | status             | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 4451     | ES          | <membershipStatus> | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
    And the user-api has the following user info
      | userId          | status | email               | brand | token | tokenType        |
      | <currentUserId> | ACTIVE | <currentUsersEmail> | ED    | null  | REQUEST_PASSWORD |
      | <newUserId>     | ACTIVE | <newUsersEmail>     | ED    | null  | REQUEST_PASSWORD |
    When the user wants to update the MemberAccount:
      | memberAccountId | userId      | operation      |
      | 123             | <newUserId> | UPDATE_USER_ID |
    Then the MemberAccount stored in db has the following information:
      | memberAccountId | userId      | firstName | lastNames |
      | 123             | <newUserId> | JOSE      | GARCIA    |
    And the number of membership subscription messages sent is 2
    And membershipSubscriptionMessage is correctly sent to kafka queue as <newUsersSubscriptionStatus> and email will be sent to <newUsersEmail>
    And membershipSubscriptionMessage is correctly sent to kafka queue as UNSUBSCRIBED and email will be sent to <currentUsersEmail>
    Examples:
      | membershipStatus   | currentUserId | newUserId | currentUsersEmail | newUsersEmail       | newUsersSubscriptionStatus |
      | ACTIVATED          | 4321          | 257       | joseg1@odigeo.com | newuser1@odigeo.com | SUBSCRIBED                 |
      | PENDING_TO_COLLECT | 8841          | 258       | joseg2@odigeo.com | newuser2@odigeo.com | PENDING                    |

  Scenario Outline: Old userId has 2 member accounts with active memberships in the same website
    Given the next memberAccount stored in db:
      | memberAccountId | userId          | firstName | lastNames |
      | 123             | <currentUserId> | JOSE      | GARCIA    |
      | 643             | <currentUserId> | JOSE      | GARCIA    |
    And the next membership stored in db:
      | memberId | website     | status             | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 4451     | ES          | ACTIVATED          | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
      | 3685     | ES          | ACTIVATED          | ENABLED     | 643             | 2017-07-06     | 2018-07-06     |
    And the user-api has the following user info
      | userId          | status | email               | brand | token | tokenType        |
      | <currentUserId> | ACTIVE | <currentUsersEmail> | ED    | null  | REQUEST_PASSWORD |
      | <newUserId>     | ACTIVE | <newUsersEmail>     | ED    | null  | REQUEST_PASSWORD |
    When the user wants to update the MemberAccount:
      | memberAccountId | userId      | operation      |
      | 123             | <newUserId> | UPDATE_USER_ID |
    Then the MemberAccount stored in db has the following information:
      | memberAccountId | userId      | firstName | lastNames |
      | 123             | <newUserId> | JOSE      | GARCIA    |
    And the number of membership subscription messages sent is 1
    And membershipSubscriptionMessage is correctly sent to kafka queue as SUBSCRIBED and email will be sent to <newUsersEmail>
    Examples:
      | currentUserId | newUserId | currentUsersEmail | newUsersEmail       |
      | 4466          | 257       | joseg3@odigeo.com | newuser3@odigeo.com |