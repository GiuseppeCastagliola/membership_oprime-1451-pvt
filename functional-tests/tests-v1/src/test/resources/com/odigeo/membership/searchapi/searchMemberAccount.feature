Feature: Test search member accounts

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 100             | 1000   | JOSE      | GOMEZ     |
      | 101             | 1100   | ANA       | GOMEZ     |
      | 102             | 1200   | JOHN      | WICK      |
      | 103             | 1300   | MICK      | JAGGER    |
      | 104             | 1400   | MICK      | BURTON    |
      | 105             | 1700   | MAX       | BIAGGI    |
      | 106             | 1700   | MAX       | BIAGGI    |

    And the next membership stored in db:
      | memberId | website | status              | autoRenewal | memberAccountId | activationDate | expirationDate | monthsDuration | balance | productStatus | totalPrice | sourceType     | membershipType |
      | 99000    | ES      | ACTIVATED           | ENABLED     | 100             | 2017-07-06     | 2018-07-06     | 12             | 54.99   | null          | 54.99      | FUNNEL_BOOKING | BASIC          |
      | 99001    | ES      | PENDING_TO_ACTIVATE | ENABLED     | 100             | null           | null           | 1              | 0       | null          | 34.99      | FUNNEL_BOOKING | BASIC          |
      | 99010    | ES      | DEACTIVATED         | ENABLED     | 101             | 2019-01-01     | 2020-01-01     | 12             | 36.30   | CONTRACT      | 54.99      | FUNNEL_BOOKING | BASIC          |
      | 99020    | OPDE    | EXPIRED             | ENABLED     | 102             | 2017-11-15     | 2018-11-15     | 12             | 0       | null          | 74.99      | FUNNEL_BOOKING | BASIC          |
      | 99021    | OPDE    | ACTIVATED           | DISABLED    | 102             | 2018-11-15     | 2019-11-15     | 12             | 18.19   | null          | 36.99      | FUNNEL_BOOKING | BASIC          |
      | 99030    | GOFR    | ACTIVATED           | ENABLED     | 103             | 2019-06-24     | 2019-09-24     | 3              | 42.50   | CONTRACT      | 0          | POST_BOOKING   | BASIC          |
      | 99040    | FR      | EXPIRED             | ENABLED     | 104             | 2019-01-10     | 2020-01-10     | 12             | 11.99   | CONTRACT      | 39.99      | FUNNEL_BOOKING | BASIC          |
      | 99041    | FR      | PENDING_TO_COLLECT  | ENABLED     | 104             | null           | 2021-01-10     | 6              | 54.99   | INIT          | 54.99      | FUNNEL_BOOKING | BUSINESS       |
      | 99050    | IT      | PENDING_TO_ACTIVATE | ENABLED     | 105             | null           | 2018-08-05     | 3              | 54.99   | null          | 0          | POST_BOOKING   | BUSINESS       |
      | 99051    | IT      | EXPIRED             | ENABLED     | 106             | 2018-08-05     | 2019-08-05     | 12             | 0.09    | CONTRACT      | 99.99      | FUNNEL_BOOKING | BUSINESS       |
      | 99052    | IT      | ACTIVATED           | ENABLED     | 106             | 2019-08-05     | 2020-08-05     | 12             | 54.99   | null          | 74.99      | FUNNEL_BOOKING | BUSINESS       |

  Scenario Outline: member account search

    When a search member account is requested with following parameters:
      | firstName   | lastName   | userId   | withMemberships   |
      | <firstName> | <lastName> | <userId> | <withMemberships> |

    Then the returned accounts by the search should include the member accounts with the following ids <accountIds>, and the following memberships <membershipIds>

    Examples:
      | firstName | lastName | userId | withMemberships | accountIds | membershipIds     |
      | JOSE      | null     | null   | false           | 100        |                   |
      | null      | GOMEZ    | null   | false           | 100;101    |                   |
      | null      | null     | 1700   | false           | 105;106    |                   |
      | ANA       | GOMEZ    | null   | false           | 101        |                   |
      | null      | GOMEZ    | 1000   | false           | 100        |                   |
      | MICK      | null     | 1400   | false           | 104        |                   |
      | JOSE      | null     | null   | true            | 100        | 99000;99001       |
      | null      | GOMEZ    | null   | true            | 100;101    | 99000;99001;99010 |
      | null      | null     | 1700   | true            | 105;106    | 99050;99051;99052 |
      | ANA       | GOMEZ    | null   | true            | 101        | 99010             |
      | null      | GOMEZ    | 1000   | true            | 100        | 99000;99001       |
      | MICK      | null     | 1400   | true            | 104        | 99040;99041       |
