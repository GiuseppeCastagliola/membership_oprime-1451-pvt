package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.functionals.database.DatabaseWorld;
import com.odigeo.membership.functionals.membership.AuditMemberAccountBuilder;
import com.odigeo.membership.request.product.creation.CreateMembershipRequest;
import com.odigeo.membership.world.MemberAccountManagementWorld;
import com.odigeo.membership.world.MembershipManagementWorld;
import cucumber.api.java.en.Then;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

@ScenarioScoped
public class AuditMemberAccountSteps {

    private final MembershipManagementWorld world;
    private final MemberAccountManagementWorld accountManagementWorld;
    private final DatabaseWorld databaseWorld;

    @Inject
    public AuditMemberAccountSteps(final MembershipManagementWorld world, MemberAccountManagementWorld accountManagementWorld, final DatabaseWorld databaseWorld) {
        this.world = world;
        this.accountManagementWorld = accountManagementWorld;
        this.databaseWorld = databaseWorld;
    }

    @Then("^the member creation is audited with this information$")
    public void thenMemberCreationAudited() throws InterruptedException, SQLException, ClassNotFoundException {
        Long memberAccountId = world.getCreatedMemberId();
        CreateMembershipRequest createMembershipRequest = accountManagementWorld.getCreateMembershipRequest();
        Map<String, String> createMembershipRequestAsMap = createMembershipRequest.getAsMap();
        List<AuditMemberAccountBuilder> memberAccountAudits = databaseWorld.getMemberAccountAudits(memberAccountId);
        AuditMemberAccountBuilder createdAudit = memberAccountAudits.iterator().next();
        AuditMemberAccountBuilder expectedAudit = new AuditMemberAccountBuilder()
                .setMemberAccountId(memberAccountId)
                .setUserId(Long.valueOf(createMembershipRequestAsMap.get("userId")))
                .setFirstName(createMembershipRequestAsMap.get("name"))
                .setLastName(createMembershipRequestAsMap.get("lastNames"));
        assertFalse(memberAccountAudits.isEmpty());
        assertEquals(memberAccountAudits.size(), 1);
        assertEquals(createdAudit, expectedAudit);
        assertNotNull(createdAudit.getTimestamp());
    }

    @Then("^the audit member account (\\d+) has userId (\\d+), the name (\\w+) and the lastName (\\w+)$")
    public void thenMemberUpdateAudited(final Long memberAccountId, final Long userId, final String name, final String lastName) throws InterruptedException, SQLException, ClassNotFoundException {
        List<AuditMemberAccountBuilder> memberAccountAudits = databaseWorld.getMemberAccountAudits(memberAccountId);
        AuditMemberAccountBuilder updatedAudit = memberAccountAudits.iterator().next();
        assertFalse(memberAccountAudits.isEmpty());
        assertEquals(memberAccountAudits.size(), 1);
        assertEquals(updatedAudit.getUserId(), userId);
        assertEquals(updatedAudit.getFirstName(), name);
        assertEquals(updatedAudit.getLastName(), lastName);
        assertNotNull(updatedAudit.getTimestamp());
    }

    @Then("^all the member account (\\d+) changes are audited$")
    public void thenMemberAccountChangesAudited(final Long memberAccountId, final List<AuditMemberAccountBuilder> expectedAudits) throws InterruptedException, SQLException, ClassNotFoundException {
        List<AuditMemberAccountBuilder> memberAccountAudits = databaseWorld.getMemberAccountAudits(memberAccountId);
        assertEquals(memberAccountAudits.size(), expectedAudits.size());
        IntStream.range(0, memberAccountAudits.size())
                .forEach(index -> assertEquals(memberAccountAudits.get(index), expectedAudits.get(index)));
    }
}
