package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.functionals.database.DatabaseWorld;
import com.odigeo.membership.functionals.membership.booking.MembershipBookingBuilder;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

import static java.math.BigDecimal.ROUND_CEILING;
import static org.apache.commons.collections.CollectionUtils.isEmpty;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

@ScenarioScoped
public class BookingTrackingSteps extends CommonSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingTrackingSteps.class);

    private final DatabaseWorld databaseWorld;

    @Inject
    public BookingTrackingSteps(final ServerConfiguration serverConfiguration, final DatabaseWorld databaseWorld) {
        super(serverConfiguration);
        this.databaseWorld = databaseWorld;
    }

    @And("^the next tracked booking stored in db:$")
    public void createMembershipInDB(final List<MembershipBookingBuilder> trackedBookings) throws InterruptedException, SQLException, ClassNotFoundException {
        databaseWorld.addTrackedBookings(trackedBookings);
    }

    @When("^the booking with the ID (\\d+) is tracked$")
    public void trackBooking(final Long bookingId) throws InvalidParametersException {
        retryBookingTracking.retryBookingTracking(bookingId);
    }

    @Then("^the tracked booking with the booking ID (\\d+) does not exist$")
    public void trackedBookingDoesNotExist(final Long bookingId) throws InterruptedException, SQLException, ClassNotFoundException {
        assertTrue(databaseWorld.findTrackedBookingsByBookingId(bookingId).isEmpty());
    }

    @Then("^the tracked booking exists in db$")
    public void trackedBookingsExist(final List<MembershipBookingBuilder> trackedBookings) throws InterruptedException, SQLException, ClassNotFoundException {
        verifyTrackedBookingsInDB(trackedBookings, false);
    }

    @Then("^after some waiting the tracked booking exists in db$")
    public void awaitedTrackedBookingsExist(final List<MembershipBookingBuilder> trackedBookings) throws InterruptedException, SQLException, ClassNotFoundException {
        verifyTrackedBookingsInDB(trackedBookings, true);
    }

    private void verifyTrackedBookingsInDB(final List<MembershipBookingBuilder> trackedBookings, final boolean withRetries)
            throws InterruptedException, SQLException, ClassNotFoundException {
        for (MembershipBookingBuilder expectedTrackedBooking : trackedBookings) {
            List<MembershipBookingBuilder> foundBookings;
            if (withRetries) {
                foundBookings = awaitTrackedBookings(20, 3000L, expectedTrackedBooking.getBookingId());
            } else {
                foundBookings = databaseWorld.findTrackedBookingsByBookingId(expectedTrackedBooking.getBookingId());
            }
            assertEquals(1, foundBookings.size());
            MembershipBookingBuilder dbTrackedBooking = foundBookings.iterator().next();
            verifyTrackedFields(expectedTrackedBooking, dbTrackedBooking);
        }
    }

    private List<MembershipBookingBuilder> awaitTrackedBookings(int maxAttempts, long sleepTimeMillis, long bookingId)
            throws InterruptedException, SQLException, ClassNotFoundException {
        List<MembershipBookingBuilder> trackedBookings = null;
        int attempts = 0;
        while (attempts < maxAttempts && isEmpty(trackedBookings)) {
            LOGGER.info("booking tracking find attempt: {}", attempts);
            trackedBookings = databaseWorld.findTrackedBookingsByBookingId(bookingId);
            if (isEmpty(trackedBookings)) {
                Thread.sleep(sleepTimeMillis);
            }
            attempts++;
        }
        return trackedBookings;
    }

    private void verifyTrackedFields(MembershipBookingBuilder expectedTracking, MembershipBookingBuilder actualTracking) {
        assertEquals(expectedTracking.getMemberId(), actualTracking.getMemberId());
        assertEquals(expectedTracking.getAvoidFeeAmount().setScale(2, ROUND_CEILING), actualTracking.getAvoidFeeAmount().setScale(2, ROUND_CEILING));
        assertEquals(expectedTracking.getCostFeeAmount().setScale(2, ROUND_CEILING), actualTracking.getCostFeeAmount().setScale(2, ROUND_CEILING));
        assertEquals(expectedTracking.getPerksFeeAmount().setScale(2, ROUND_CEILING), actualTracking.getPerksFeeAmount().setScale(2, ROUND_CEILING));
        assertEquals(expectedTracking.getBookingDate(), actualTracking.getBookingDate());
    }
}
