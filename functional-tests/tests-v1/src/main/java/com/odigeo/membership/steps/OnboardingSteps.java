package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.functionals.database.DatabaseInstallerWorld;
import com.odigeo.membership.functionals.membership.OnboardingBuilder;
import com.odigeo.membership.mocks.database.stores.OnboardingStore;
import com.odigeo.membership.request.onboarding.OnboardingDevice;
import com.odigeo.membership.request.onboarding.OnboardingEvent;
import com.odigeo.membership.request.onboarding.OnboardingEventRequest;
import com.odigeo.membership.world.OnboardingWorld;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

@ScenarioScoped
public class OnboardingSteps extends CommonSteps {

    private final OnboardingWorld onboardingWorld;
    private final OnboardingStore onboardingStore;
    private final DatabaseInstallerWorld databaseInstallerWorld;

    @Inject
    public OnboardingSteps(ServerConfiguration serverConfiguration, OnboardingWorld onboardingWorld, OnboardingStore onboardingStore, DatabaseInstallerWorld databaseInstallerWorld) {
        super(serverConfiguration);
        this.onboardingWorld = onboardingWorld;
        this.onboardingStore = onboardingStore;
        this.databaseInstallerWorld = databaseInstallerWorld;
    }

    @When("^the client request saveOnboarding to API$")
    public void theClientRequestSaveOnboardingToAPI(List<OnboardingBuilder> onboardingData) {
        OnboardingBuilder onboardingToCreate = onboardingData.iterator().next();
        OnboardingEventRequest request = new OnboardingEventRequest.Builder()
                .withMemberAccountId(onboardingToCreate.getMemberAccountId())
                .withEvent(OnboardingEvent.valueOf(onboardingToCreate.getEvent()))
                .withDevice(OnboardingDevice.valueOf(onboardingToCreate.getDevice()))
                .build();
        onboardingWorld.setCreateRequest(request);
        onboardingWorld.setCreatedOnboardingId(onboardingService.saveOnboardingEvent(request));
    }

    @Then("^the onboarding is correctly saved$")
    public void theOnboardingIsCorrectlySaved() throws InterruptedException, SQLException, ClassNotFoundException {
        OnboardingStore.Onboarding savedOnboarding = onboardingStore.getOnboardings(databaseInstallerWorld.getConnection()).iterator().next();
        assertEquals(savedOnboarding.getId(), onboardingWorld.getCreatedOnboardingId().longValue());
        assertEquals(savedOnboarding.getMemberAccountId(), onboardingWorld.getCreateRequest().getMemberAccountId());
        assertEquals(savedOnboarding.getEvent(), onboardingWorld.getCreateRequest().getEvent().toString());
        assertEquals(savedOnboarding.getDevice(), onboardingWorld.getCreateRequest().getDevice().name());
        assertNotNull(savedOnboarding.getTimestamp());
    }
}
