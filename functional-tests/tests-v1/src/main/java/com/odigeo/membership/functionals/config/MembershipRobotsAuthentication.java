package com.odigeo.membership.functionals.config;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;


@Singleton
@ConfiguredInPropertiesFile
public class MembershipRobotsAuthentication {
    private String client;
    private String secretKey;

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
}
