package com.odigeo.membership.member.nosql;

import com.edreams.base.DataAccessException;

public interface MembershipNoSQLRepository {

    void store(String key, String value, int secondsToExpire) throws DataAccessException;

    String get(String key) throws DataAccessException;
}
