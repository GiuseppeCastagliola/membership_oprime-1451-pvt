package com.odigeo.membership.member;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.db.DbUtils;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.member.util.MemberAccountEntityBuilder;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class MemberAccountStore {

    private static final Logger logger = Logger.getLogger(MemberAccountStore.class);

    private static final String MISSING_MEMBER_ACCOUNT_WITH_ID = "Missing memberAccount with memberAccountId: ";

    static final String MEMBER_ACCOUNT_FIELDS = "a.ID as ACCOUNT_ID,a.USER_ID,a.FIRST_NAME,a.LAST_NAME, a.TIMESTAMP ";
    private static final String MEMBERSHIP_FIELDS = MembershipStore.MEMBERSHIP_FIELDS;
    private static final String JOIN_RECURRING_TABLE = "left outer join MEMBERSHIP_OWN.GE_MEMBERSHIP_RECURRING gmr on gmr.MEMBERSHIP_ID = m.ID ";

    private static final String ADD_MEMBER_ACCOUNT_SQL = "INSERT INTO GE_MEMBER_ACCOUNT(ID,USER_ID,FIRST_NAME,LAST_NAME) VALUES (?, ?, ?, ?) ";
    private static final String SELECT_NEXT_MEMBER_ACCOUNT_ID = "SELECT SEQ_GE_MEMBER_ACCOUNT_ID.nextval FROM dual ";
    private static final String UPDATE_MEMBER_ACCOUNT_SQL = "UPDATE GE_MEMBER_ACCOUNT SET FIRST_NAME = ?, LAST_NAME = ? WHERE ID = ?";
    private static final String UPDATE_USER_ID_SQL = "UPDATE GE_MEMBER_ACCOUNT SET USER_ID = ? WHERE ID = ?";

    private static final String GET_MEMBER_ACCOUNT_BY_ID_SQL = "SELECT " + MEMBER_ACCOUNT_FIELDS + " FROM GE_MEMBER_ACCOUNT a WHERE a.ID = ? ";
    private static final String GET_MEMBER_ACCOUNT_AND_MEMBERSHIPS_BY_STATUS = "SELECT gmr.RECURRING_ID, " + MEMBER_ACCOUNT_FIELDS + ", " + MEMBERSHIP_FIELDS
            + "FROM GE_MEMBER_ACCOUNT a JOIN GE_MEMBERSHIP m ON a.id=m.MEMBER_ACCOUNT_ID " + "AND a.USER_ID = ? AND m.STATUS = ? " + JOIN_RECURRING_TABLE + "order by m.EXPIRATION_DATE DESC";

    private static final String GET_MEMBER_ACCOUNT_AND_MEMBERSHIPS_BY_ACCOUNT_ID = "SELECT gmr.RECURRING_ID, " + MEMBER_ACCOUNT_FIELDS + ", " + MEMBERSHIP_FIELDS
            + "FROM GE_MEMBER_ACCOUNT a JOIN GE_MEMBERSHIP m ON a.id=m.MEMBER_ACCOUNT_ID AND a.ID = ? "
            + JOIN_RECURRING_TABLE + "order by m.EXPIRATION_DATE DESC";


    List<MemberAccount> getMemberAccountAndMembershipsActivated(DataSource dataSource, long userId) throws SQLException {
        return getMemberAccountAndMembershipsByStatus(dataSource, userId, MemberStatus.ACTIVATED);
    }

    public List<MemberAccount> getMemberAccountAndMembershipsByStatus(DataSource dataSource, long userId, MemberStatus status) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_MEMBER_ACCOUNT_AND_MEMBERSHIPS_BY_STATUS)) {
            preparedStatement.setLong(1, userId);
            preparedStatement.setString(2, status.toString());
            try (ResultSet rs = preparedStatement.executeQuery()) {
                return getMemberAccountEntityBuilder().buildWithMembership(rs);
            }
        }
    }

    public MemberAccount getMemberAccountById(DataSource dataSource, long memberAccountId) throws SQLException, DataNotFoundException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_MEMBER_ACCOUNT_BY_ID_SQL)) {
            preparedStatement.setLong(1, memberAccountId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    return getMemberAccountEntityBuilder().build(rs, "ACCOUNT_ID");
                }
            }
        }
        logger.warn(MISSING_MEMBER_ACCOUNT_WITH_ID + memberAccountId);
        throw new DataNotFoundException(MISSING_MEMBER_ACCOUNT_WITH_ID + memberAccountId);
    }

    public long createMemberAccount(DataSource dataSource, Long userId, String name, String lastNames) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_MEMBER_ACCOUNT_SQL)) {
            final long nextMemberAccountId = DbUtils.nextSequence(connection.prepareStatement(SELECT_NEXT_MEMBER_ACCOUNT_ID));
            preparedStatement.setLong(1, nextMemberAccountId);
            preparedStatement.setLong(2, userId);
            preparedStatement.setString(3, name);
            preparedStatement.setString(4, lastNames);
            preparedStatement.execute();
            return nextMemberAccountId;
        }
    }

    Boolean updateMemberAccountNames(DataSource dataSource, Long memberAccountId, String name, String lastNames) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_MEMBER_ACCOUNT_SQL)) {
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, lastNames);
            preparedStatement.setLong(3, memberAccountId);
            return (preparedStatement.executeUpdate() > 0);
        }
    }

    public Boolean updateMemberAccountUserId(DataSource dataSource, Long memberAccountId, Long userId) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_ID_SQL)) {
            preparedStatement.setString(1, userId.toString());
            preparedStatement.setLong(2, memberAccountId);
            return (preparedStatement.executeUpdate() > 0);
        }
    }

    public MemberAccount getMemberAccountWithMembershipById(DataSource dataSource, long memberAccountId) throws SQLException, DataNotFoundException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_MEMBER_ACCOUNT_AND_MEMBERSHIPS_BY_ACCOUNT_ID)) {
            preparedStatement.setLong(1, memberAccountId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                List<MemberAccount> memberAccounts = getMemberAccountEntityBuilder().buildWithMembership(rs);
                if (memberAccounts.isEmpty()) {
                    logger.warn(MISSING_MEMBER_ACCOUNT_WITH_ID + memberAccountId);
                    throw new DataNotFoundException(MISSING_MEMBER_ACCOUNT_WITH_ID + memberAccountId);
                } else {
                    return memberAccounts.get(0);
                }
            }
        }
    }

    @SuppressFBWarnings("SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING")
    public List<MemberAccount> searchMemberAccounts(DataSource dataSource, MemberAccountSearch memberAccountSearch) throws SQLException {
        List<MemberAccount> memberAccounts = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(memberAccountSearch.getQueryString())) {
            int preparedIndex = 1;
            for (Object value : memberAccountSearch.getValues()) {
                preparedStatement.setObject(preparedIndex++, value);
            }

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (memberAccountSearch.isWithMemberships()) {
                    memberAccounts = getMemberAccountEntityBuilder().buildWithMembership(resultSet);
                } else {
                    while (resultSet.next()) {
                        memberAccounts.add(getMemberAccountEntityBuilder().build(resultSet, "ACCOUNT_ID"));
                    }
                }
            }
        }
        return memberAccounts;
    }

    private MemberAccountEntityBuilder getMemberAccountEntityBuilder() {
        return ConfigurationEngine.getInstance(MemberAccountEntityBuilder.class);
    }

}
