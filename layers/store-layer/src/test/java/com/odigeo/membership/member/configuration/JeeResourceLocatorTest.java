package com.odigeo.membership.member.configuration;

import com.odigeo.membership.exception.UnavailableDataSourceException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class JeeResourceLocatorTest {

    private JeeResourceLocator jeeResourceLocator;

    @BeforeMethod
    public void setUp() {
        this.jeeResourceLocator = JeeResourceLocator.getInstance();
    }

    @Test
    public void testGetInstance() {
        assertNotNull(JeeResourceLocator.getInstance());
    }

    @Test(expectedExceptions = UnavailableDataSourceException.class,
            expectedExceptionsMessageRegExp = "Unavailable DataSource: " + JeeResourceLocator.DEFAULT_DATASOURCE_NAME)
    public void testGetDefaultDataSourceThrowsUDSEIfNoDataSourceAvailable() throws Exception {
        jeeResourceLocator.getDefaultDataSource();
    }
}
