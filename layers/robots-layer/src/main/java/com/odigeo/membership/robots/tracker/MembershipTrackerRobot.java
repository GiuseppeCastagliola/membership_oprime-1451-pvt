package com.odigeo.membership.robots.tracker;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.robots.BookingUpdatesReaderService;
import com.odigeo.robots.AbstractRobot;
import com.odigeo.robots.RobotExecutionResult;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.Objects;

public class MembershipTrackerRobot extends AbstractRobot {

    private static final Logger LOGGER = Logger.getLogger(MembershipTrackerRobot.class);
    private BookingUpdatesReaderService bookingUpdateService;

    @Inject
    public MembershipTrackerRobot(@Assisted String robotId) {
        super(robotId);
    }

    @Override
    public RobotExecutionResult execute(Map<String, String> configuration) {
        final boolean success = getBookingUpdatesReaderService().startTrackerConsumerController();
        LOGGER.info("Reader finished successfully: " + success);
        return success ? RobotExecutionResult.OK : RobotExecutionResult.CRITICAL;
    }

    @Override
    public void interrupt() {
        getBookingUpdatesReaderService().stopTrackerConsumerController();
        super.interrupt();
        LOGGER.info("Robot interrupted successfully");
    }

    private BookingUpdatesReaderService getBookingUpdatesReaderService() {
        if (Objects.isNull(bookingUpdateService)) {
            bookingUpdateService = ConfigurationEngine.getInstance(BookingUpdatesReaderService.class);
        }
        return bookingUpdateService;
    }

}
