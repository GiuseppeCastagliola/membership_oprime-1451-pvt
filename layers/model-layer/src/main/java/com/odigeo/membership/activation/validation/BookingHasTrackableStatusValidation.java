package com.odigeo.membership.activation.validation;

import com.odigeo.bookingapi.v12.responses.BookingDetail;

import java.util.Arrays;
import java.util.List;

public class BookingHasTrackableStatusValidation extends AbstractValidation<BookingDetail> {

    private final List<String> trackableStatuses = Arrays.asList("CONTRACT", "REQUEST", "HOLD");

    public BookingHasTrackableStatusValidation(BookingDetail o) {
        super(o);
    }

    @Override
    public boolean validate() {
        return trackableStatuses.contains(o.getBookingStatus());
    }

}
