package com.odigeo.membership;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class BookingTracking implements Serializable {
    private long bookingId;
    private long membershipId;
    private LocalDateTime bookingDate;
    private BigDecimal avoidFeeAmount;
    private BigDecimal costFeeAmount;
    private BigDecimal perksAmount;

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public long getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(long membershipId) {
        this.membershipId = membershipId;
    }

    public LocalDateTime getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(LocalDateTime bookingDate) {
        this.bookingDate = bookingDate;
    }

    public BigDecimal getAvoidFeeAmount() {
        return avoidFeeAmount;
    }

    public void setAvoidFeeAmount(BigDecimal avoidFeeAmount) {
        this.avoidFeeAmount = avoidFeeAmount;
    }

    public BigDecimal getCostFeeAmount() {
        return costFeeAmount;
    }

    public void setCostFeeAmount(BigDecimal costFeeAmount) {
        this.costFeeAmount = costFeeAmount;
    }

    public BigDecimal getPerksAmount() {
        return perksAmount;
    }

    public void setPerksAmount(BigDecimal perksAmount) {
        this.perksAmount = perksAmount;
    }
}
