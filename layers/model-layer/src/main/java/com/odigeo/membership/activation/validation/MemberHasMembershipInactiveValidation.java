package com.odigeo.membership.activation.validation;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;

/**
 * Created by octavi.ripolles on 25/08/2017.
 */
public class MemberHasMembershipInactiveValidation extends AbstractValidation<Membership> {

    public MemberHasMembershipInactiveValidation(Membership o) {
        super(o);
    }

    public boolean validate() {
        return MemberStatus.PENDING_TO_ACTIVATE.equals(o.getStatus());
    }
}
