package com.odigeo.membership.onboarding;

public enum OnboardingDevice {

    APP, MOBILE, DESKTOP
}
