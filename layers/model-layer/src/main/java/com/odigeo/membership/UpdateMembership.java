package com.odigeo.membership;

public class UpdateMembership {
    private String membershipId;
    private String operation;
    private String membershipStatus;
    private String membershipRenewal;
    private Long userCreditCardId;
    private String recurringId;

    public String getMembershipId() {
        return membershipId;
    }

    public UpdateMembership setMembershipId(String membershipId) {
        this.membershipId = membershipId;
        return this;
    }

    public String getOperation() {
        return operation;
    }

    public UpdateMembership setOperation(String operation) {
        this.operation = operation;
        return this;
    }

    public String getMembershipStatus() {
        return membershipStatus;
    }

    public UpdateMembership setMembershipStatus(String membershipStatus) {
        this.membershipStatus = membershipStatus;
        return this;
    }

    public String getMembershipRenewal() {
        return membershipRenewal;
    }

    public UpdateMembership setMembershipRenewal(String membershipRenewal) {
        this.membershipRenewal = membershipRenewal;
        return this;
    }

    public Long getUserCreditCardId() {
        return userCreditCardId;
    }

    public UpdateMembership setUserCreditCardId(Long userCreditCardId) {
        this.userCreditCardId = userCreditCardId;
        return this;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public UpdateMembership setRecurringId(String recurringId) {
        this.recurringId = recurringId;
        return this;
    }
}
