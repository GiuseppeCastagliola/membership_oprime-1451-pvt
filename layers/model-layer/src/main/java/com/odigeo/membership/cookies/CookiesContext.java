package com.odigeo.membership.cookies;

import java.io.Serializable;

public class CookiesContext implements Serializable {

    private final String outerDomain;
    private final CookiesStorage storage;

    public CookiesContext(String outerDomain) {
        this.outerDomain = outerDomain;
        storage = new CookiesStorage();
    }

    public CookiesStorage getStorage() {
        return storage;
    }

    public String getOuterDomain() {
        return outerDomain;
    }
}
