package com.odigeo.membership.activation.validation;

public interface Validation {
    boolean validate();
}
