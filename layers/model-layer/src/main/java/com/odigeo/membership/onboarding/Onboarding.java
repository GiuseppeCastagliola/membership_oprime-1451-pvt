package com.odigeo.membership.onboarding;

import java.io.Serializable;

public class Onboarding implements Serializable {

    private final Long memberAccountId;
    private final OnboardingEvent event;
    private final OnboardingDevice device;

    public Onboarding(Builder builder) {
        this.memberAccountId = builder.getMemberAccountId();
        this.event = builder.getEvent();
        this.device = builder.getDevice();
    }

    public Long getMemberAccountId() {
        return memberAccountId;
    }

    public OnboardingEvent getEvent() {
        return event;
    }

    public OnboardingDevice getDevice() {
        return device;
    }

    public static class Builder {

        private Long memberAccountId;
        private OnboardingEvent event;
        private OnboardingDevice device;

        public Long getMemberAccountId() {
            return memberAccountId;
        }

        public Builder withMemberAccountId(Long memberAccountId) {
            this.memberAccountId = memberAccountId;
            return this;
        }

        public OnboardingEvent getEvent() {
            return event;
        }

        public Builder withEvent(OnboardingEvent event) {
            this.event = event;
            return this;
        }

        public OnboardingDevice getDevice() {
            return device;
        }

        public Builder withDevice(OnboardingDevice device) {
            this.device = device;
            return this;
        }

        public Onboarding build() {
            return new Onboarding(this);
        }
    }

    @Override
    public String toString() {
        return "Onboarding{"
                + ", memberAccountId=" + memberAccountId
                + ", event=" + event
                + ", device=" + device
                + '}';
    }
}
