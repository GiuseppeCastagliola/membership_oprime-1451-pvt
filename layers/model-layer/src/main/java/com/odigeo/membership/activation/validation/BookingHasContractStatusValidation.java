package com.odigeo.membership.activation.validation;

import com.odigeo.bookingapi.v12.responses.BookingDetail;

public class BookingHasContractStatusValidation extends AbstractValidation<BookingDetail> {

    public BookingHasContractStatusValidation(BookingDetail o) {
        super(o);
    }

    @Override
    public boolean validate() {
        return "CONTRACT".equals(o.getBookingStatus());
    }

}
