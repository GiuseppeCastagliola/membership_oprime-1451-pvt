package com.odigeo.membership.mapper;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public class GeneralMapperCreator implements MapperCreator {

    public MapperFactory getMapperFactory() {
        return new DefaultMapperFactory.Builder().build();
    }

    public MapperFacade getMapper() {
        return getMapperFactory().getMapperFacade();
    }
}
