package com.odigeo.membership;

import java.math.BigDecimal;
import java.util.Objects;

public class MembershipFee {
    private String membershipId;
    private BigDecimal amount;
    private String currency;
    private String feeType;
    private String subCode;

    public MembershipFee(String membershipId, BigDecimal amount, String currency, String feeType) {
        this.membershipId = membershipId;
        this.amount = amount;
        this.currency = currency;
        this.feeType = feeType;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getSubCode() {
        return this.subCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MembershipFee that = (MembershipFee) o;
        return Objects.equals(membershipId, that.membershipId)
                && Objects.equals(amount, that.amount)
                && Objects.equals(currency, that.currency)
                && Objects.equals(feeType, that.feeType)
                && Objects.equals(subCode, that.subCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(membershipId, amount, currency, feeType, subCode);
    }
}
