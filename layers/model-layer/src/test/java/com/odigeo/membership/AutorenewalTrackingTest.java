package com.odigeo.membership;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class AutorenewalTrackingTest {

    private static final String REQUESTER = "requester-with-very-large-identifier";
    private static final String REQUESTED_METHOD = "requested-method-with-very-large-identifier";

    @Test
    public void testAutorenewalTrackingBuilder() {
        AutorenewalTracking autorenewalTracking = new AutorenewalTracking();
        autorenewalTracking.setRequestedMethod(REQUESTED_METHOD);
        autorenewalTracking.setRequester(REQUESTER);

        assertNotNull(autorenewalTracking.getTimestamp());
        assertEquals(autorenewalTracking.getRequestedMethod(), "REQUESTED-METHOD-WITH-VER");
        assertEquals(autorenewalTracking.getRequester(), "REQUESTER-WITH-VERY-LARGE");
    }
}