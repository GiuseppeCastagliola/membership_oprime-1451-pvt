package com.odigeo.membership.auth;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.util.HashSet;

public class AppPermissionTest extends BeanTest<AppPermission> {

    @Override
    protected AppPermission getBean() {
        return assembleAppPermission();
    }

    private AppPermission assembleAppPermission() {
        AppPermission appPermission = new AppPermission();
        appPermission.setPermissions(new HashSet<>());
        return appPermission;
    }

    @Test
    public void appPermissionEqualsVerifierTest() {
        EqualsVerifier.forClass(AppPermission.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}