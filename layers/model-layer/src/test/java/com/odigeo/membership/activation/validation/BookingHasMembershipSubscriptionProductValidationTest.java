package com.odigeo.membership.activation.validation;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.MembershipSubscriptionBooking;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Created by octavi.ripolles on 14/09/2017.
 */
public class BookingHasMembershipSubscriptionProductValidationTest {

    @Test
    public void testValidation() {
        BookingDetail bookingDetail = new BookingDetail();
        BookingHasMembershipSubscriptionProductValidation bookingHasMembershipSubscriptionProductValidation = new BookingHasMembershipSubscriptionProductValidation(bookingDetail);

        // Checking for null
        assertFalse(bookingHasMembershipSubscriptionProductValidation.validate());

        bookingDetail.setBookingProducts(new ArrayList<>());

        assertFalse(bookingHasMembershipSubscriptionProductValidation.validate());

        MembershipSubscriptionBooking membershipSubscriptionBooking = new MembershipSubscriptionBooking();
        bookingDetail.getBookingProducts().add(membershipSubscriptionBooking);

        assertFalse(bookingHasMembershipSubscriptionProductValidation.validate());

        membershipSubscriptionBooking.setProductType("MEMBERSHIP_SUBSCRIPTION");
        assertTrue(bookingHasMembershipSubscriptionProductValidation.validate());
    }
}
