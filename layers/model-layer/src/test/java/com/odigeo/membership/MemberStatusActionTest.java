package com.odigeo.membership;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.util.Date;

import static org.testng.Assert.assertNull;

public class MemberStatusActionTest extends BeanTest<MemberStatusAction> {

    private static final Long ID = 1L;
    private static final Long MEMBER_ID = 123L;
    private static final StatusAction STATUS_ACTION = StatusAction.CREATION;
    private static final Date ACTION_DATE = new Date();


    @Override
    protected MemberStatusAction getBean() {
        return new MemberStatusAction(ID, MEMBER_ID, STATUS_ACTION, ACTION_DATE);
    }

    @Test
    public void testNullDates() {
        MemberStatusAction memberStatusAction = new MemberStatusAction(ID, MEMBER_ID, STATUS_ACTION, null);
        assertNull(memberStatusAction.getTimestamp());
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(MemberStatusAction.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}