UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.68.2' where ID ='UNL-5979/01.delete_duplicated_membership_ids.dml.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.68.2' where ID ='UNL-5979/02.create_new_membership_recurring_pk.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.68.2' where ID ='rollback/R02.create_new_membership_recurring_pk.ddl.sql';
INSERT INTO GE_RELEASE_LOG (ID,EXECUTION_DATE,MODULE) values('1.68.2',systimestamp,'membership');
