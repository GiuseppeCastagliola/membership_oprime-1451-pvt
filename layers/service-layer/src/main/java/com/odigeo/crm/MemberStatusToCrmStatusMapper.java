package com.odigeo.crm;

import com.google.inject.Singleton;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.v4.messages.SubscriptionStatus;

@Singleton
public class MemberStatusToCrmStatusMapper {

    public SubscriptionStatus mapForUpdatedMembership(Membership membership, MemberStatus newStatusApplied) {
        SubscriptionStatus subscriptionStatus = SubscriptionStatus.UNSUBSCRIBED;
        if (MemberStatus.EXPIRED.equals(newStatusApplied)) {
            subscriptionStatus = membership.isRenewable() ? SubscriptionStatus.PENDING : SubscriptionStatus.UNSUBSCRIBED;
        } else if (MemberStatus.ACTIVATED.equals(newStatusApplied)) {
            subscriptionStatus = SubscriptionStatus.SUBSCRIBED;
        }
        return subscriptionStatus;
    }

    public SubscriptionStatus map(MemberStatus memberStatus) {
        SubscriptionStatus subscriptionStatus = SubscriptionStatus.UNSUBSCRIBED;
        if (MemberStatus.ACTIVATED.equals(memberStatus)) {
            subscriptionStatus = SubscriptionStatus.SUBSCRIBED;
        } else if (MemberStatus.PENDING_TO_COLLECT.equals(memberStatus)) {
            subscriptionStatus = SubscriptionStatus.PENDING;
        }
        return subscriptionStatus;
    }
}
