package com.odigeo.membership.tracking;

import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.Membership;
import com.odigeo.membership.booking.BookingDetailWrapper;
import com.odigeo.membership.exception.RetryableException;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Optional;

import static com.odigeo.membership.booking.MembershipVatModelFee.AVOID_FEES;
import static com.odigeo.membership.booking.MembershipVatModelFee.PERKS;

public class BookingTrackingOldModel extends AbstractBookingTracking {

    public BookingTrackingOldModel(Membership membership, BookingDetailWrapper bookingDetailWrapper) {
        super(membership, bookingDetailWrapper);
    }

    @Override
    public Optional<BookingTracking> getBookingTracking() {
        return Optional.of(bookingDetailWrapper.createBookingTracking(membership.getId()));
    }

    @Override
    public void deleteTrackedBookingAndRestoreBalance(DataSource dataSource) throws RetryableException {
        Long bookingId = bookingDetailWrapper.getBookingDetail().getBookingBasicInfo().getId();
        try {
            decreaseTrackedBookingsMetric();
            getBookingTrackingStore().removeTrackedBooking(dataSource, bookingId);
        } catch (SQLException e) {
            throw new RetryableException("Cannot remove tracking for booking " + bookingId + AND_MEMBER + membership.getId(), e);
        }
    }

    @Override
    public boolean areFeesAmountChanged(BookingTracking trackedBooking) {
        BigDecimal bookingAvoidFee = bookingDetailWrapper.calculateFeeAmount(AVOID_FEES);
        BigDecimal bookingPerks = bookingDetailWrapper.calculateFeeAmount(PERKS);
        return trackedBooking.getAvoidFeeAmount().compareTo(bookingAvoidFee) != 0
            || trackedBooking.getPerksAmount().compareTo(bookingPerks) != 0;
    }
}
