package com.odigeo.membership.tracking;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.Membership;
import com.odigeo.membership.booking.BookingDetailWrapper;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.member.MembershipStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Optional;

public abstract class AbstractBookingTracking {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractBookingTracking.class);
    protected static final String AND_MEMBER = " and member ";

    protected Membership membership;
    protected BookingDetailWrapper bookingDetailWrapper;

    public AbstractBookingTracking(Membership membership, BookingDetailWrapper bookingDetailWrapper) {
        this.membership = membership;
        this.bookingDetailWrapper = bookingDetailWrapper;
    }

    public void trackBookingAndUpdateBalance(DataSource dataSource) throws RetryableException {
        long bookingId = bookingDetailWrapper.getBookingId();
        try {
            Optional<BookingTracking> alreadyTrackedBooking = Optional.ofNullable(getBookingTrackingStore().getBookingTracked(dataSource, bookingId));
            if (alreadyTrackedBooking.isPresent()) {
                deleteAndRestoreIfFeesAmountChanged(dataSource, alreadyTrackedBooking.get());
            } else {
                trackNewBooking(dataSource, bookingId);
            }
        } catch (SQLException e) {
            throw new RetryableException("SQL Exception tracking booking " + bookingId + " of membership " + membership.getId(), e);
        }
    }

    private void deleteAndRestoreIfFeesAmountChanged(DataSource dataSource, BookingTracking trackedBooking)
        throws RetryableException, SQLException {
        if (areFeesAmountChanged(trackedBooking)) {
            LOGGER.info("Fees amount changed, already tracked booking {}", trackedBooking.getBookingId());
            deleteTrackedBookingAndRestoreBalance(dataSource);
            trackNewBooking(dataSource, trackedBooking.getBookingId());
        } else {
            LOGGER.info("Booking {} already tracked, fees not changed, nothing to do", trackedBooking.getBookingId());
        }
    }

    private void trackNewBooking(DataSource dataSource, long bookingId) throws RetryableException, SQLException {
        Optional<BookingTracking> newBookingToTrack = getBookingTracking();
        if (newBookingToTrack.isPresent()) {
            BigDecimal newBalance = createTrackingAndUpdateBalance(dataSource, newBookingToTrack.get());
            LOGGER.info("Booking {} tracked and balance updated to {}", bookingId, newBalance);
        }
    }

    private BigDecimal createTrackingAndUpdateBalance(DataSource dataSource, BookingTracking newBookingToTrack) throws SQLException {
        getBookingTrackingStore().trackBooking(dataSource, newBookingToTrack);
        increaseTrackedBookingMetric();
        BigDecimal newBalance = membership.getBalance().add(newBookingToTrack.getAvoidFeeAmount());
        getMembershipStore().updateMembershipBalance(dataSource, membership.getId(), newBalance);
        return newBalance;
    }

    public abstract void deleteTrackedBookingAndRestoreBalance(DataSource dataSource) throws RetryableException;

    public abstract Optional<BookingTracking> getBookingTracking() throws RetryableException;

    public abstract boolean areFeesAmountChanged(BookingTracking bookingTracked);

    private void increaseTrackedBookingMetric() {
        MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.TRACKED_BOOKINGS_NUMBER), MetricsNames.METRICS_REGISTRY_NAME);
    }

    protected void decreaseTrackedBookingsMetric() {
        MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.TRACKED_BOOKINGS_NUMBER), MetricsNames.METRICS_REGISTRY_NAME, -1L);
    }

    protected MembershipStore getMembershipStore() {
        return ConfigurationEngine.getInstance(MembershipStore.class);
    }

    protected BookingTrackingStore getBookingTrackingStore() {
        return ConfigurationEngine.getInstance(BookingTrackingStore.class);
    }
}
