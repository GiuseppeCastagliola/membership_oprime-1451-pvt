package com.odigeo.membership.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class RetryableException extends Exception {

    public RetryableException(String message) {
        super(message);
    }

    public RetryableException(String message, Throwable e) {
        super(message, e);
    }

    public RetryableException(Throwable e) {
        super(e);
    }
}
