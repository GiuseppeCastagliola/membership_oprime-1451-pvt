package com.odigeo.membership.propertiesconfig;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.edreams.persistance.cache.Cache;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class PropertiesCacheConfiguration {

    private Cache isSendingIdsToKafkaActiveCache;
    private Cache isTransactionalWelcomeEmailActiveCache;

    public Cache getIsSendingIdsToKafkaActiveCache() {
        return isSendingIdsToKafkaActiveCache;
    }

    public void setIsSendingIdsToKafkaActiveCache(final Cache isSendingIdsToKafkaActiveCache) {
        this.isSendingIdsToKafkaActiveCache = isSendingIdsToKafkaActiveCache;
    }

    public Cache getIsTransactionalWelcomeEmailActiveCache() {
        return isTransactionalWelcomeEmailActiveCache;
    }

    public void setIsTransactionalWelcomeEmailActiveCache(Cache isTransactionalWelcomeEmailActiveCache) {
        this.isTransactionalWelcomeEmailActiveCache = isTransactionalWelcomeEmailActiveCache;
    }
}
