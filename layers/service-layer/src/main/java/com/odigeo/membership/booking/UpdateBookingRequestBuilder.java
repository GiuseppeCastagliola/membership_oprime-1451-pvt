package com.odigeo.membership.booking;

import com.odigeo.bookingapi.v12.requests.CreateBookingProductCategoryRequest;
import com.odigeo.bookingapi.v12.requests.CreateOtherProductProviderBooking;
import com.odigeo.bookingapi.v12.requests.CreateProviderPaymentDetailRequest;
import com.odigeo.bookingapi.v12.requests.FeeRequest;
import com.odigeo.bookingapi.v12.requests.Money;
import com.odigeo.bookingapi.v12.requests.PaymentType;
import com.odigeo.bookingapi.v12.requests.ProductCategoryBookingItemType;
import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.nonNull;

public class UpdateBookingRequestBuilder {
    private List<FeeRequest> feeRequests;
    private List<CreateBookingProductCategoryRequest> createBookingProductCategoryRequests;

    public UpdateBookingRequestBuilder setFeeRequests(FeeRequest... feeRequests) {
        this.feeRequests = Arrays.asList(feeRequests);
        return this;
    }

    public UpdateBookingRequestBuilder createBookingProductCategoryRequests(CreateBookingProductCategoryRequest... createProducts) {
        createBookingProductCategoryRequests = Arrays.asList(createProducts);
        return this;
    }

    public UpdateBookingRequest build() {
        UpdateBookingRequest updateBookingRequest = new UpdateBookingRequest();
        updateBookingRequest.setBookingFees(feeRequests);
        return updateBookingRequest;
    }

    public UpdateBookingRequest buildAddProduct() {
        UpdateBookingRequest updateBookingRequest = new UpdateBookingRequest();
        updateBookingRequest.setCreateBookingProductCategoryList(createBookingProductCategoryRequests);
        return updateBookingRequest;
    }

    public static class CreateBookingProductCategoryRequestBuilder {
        private final ProductCategoryBookingItemType productCategoryBookingItemType;
        private List<FeeRequest> feeRequests;
        private CreateOtherProductProviderBookingBuilder createOtherProductProviderBookingBuilder;


        public CreateBookingProductCategoryRequestBuilder feeRequests(FeeRequest... feeRequests) {
            this.feeRequests = Arrays.asList(feeRequests);
            return this;
        }

        CreateOtherProductProviderBookingBuilder getCreateOtherProductProviderBookingBuilder() {
            return createOtherProductProviderBookingBuilder;
        }

        public CreateBookingProductCategoryRequestBuilder(ProductCategoryBookingItemType productCategoryBookingItemType) {
            this.productCategoryBookingItemType = productCategoryBookingItemType;
        }

        public CreateBookingProductCategoryRequest build() {
            CreateBookingProductCategoryRequest createBookingProductCategoryRequest = new CreateBookingProductCategoryRequest();
            createBookingProductCategoryRequest.setProductCategoryBookingItemType(productCategoryBookingItemType);
            createBookingProductCategoryRequest.setProductCategoryFees(feeRequests);
            if (nonNull(getCreateOtherProductProviderBookingBuilder())) {
                createBookingProductCategoryRequest.setCreateProviderBookingList(Collections.singletonList(getCreateOtherProductProviderBookingBuilder().build()));
            }
            return createBookingProductCategoryRequest;
        }

        public CreateBookingProductCategoryRequestBuilder createOtherProductProviderBookingBuilder(CreateOtherProductProviderBookingBuilder createOtherProductProviderBookingBuilder) {
            this.createOtherProductProviderBookingBuilder = createOtherProductProviderBookingBuilder;
            return this;
        }
    }

    public static class CreateProviderPaymentDetailRequestBuilder {
        private final PaymentType paymentType;
        private final BigDecimal amount;
        private boolean merchant;
        private final String currency;

        public CreateProviderPaymentDetailRequestBuilder(PaymentType paymentType, BigDecimal amount, String currency) {
            this.paymentType = paymentType;
            this.amount = amount;
            this.currency = currency;
        }

        public CreateProviderPaymentDetailRequestBuilder merchant(boolean merchant) {
            this.merchant = merchant;
            return this;
        }

        public CreateProviderPaymentDetailRequest build() {
            CreateProviderPaymentDetailRequest createProviderPaymentDetailRequest = new CreateProviderPaymentDetailRequest();
            createProviderPaymentDetailRequest.setPaymentType(paymentType);
            createProviderPaymentDetailRequest.setAmount(amount);
            createProviderPaymentDetailRequest.setCurrency(currency);
            createProviderPaymentDetailRequest.setMerchant(merchant);
            return createProviderPaymentDetailRequest;
        }
    }

    public static class CreateOtherProductProviderBookingBuilder {
        private final Status status;
        private final BigDecimal amount;
        private final String currency;
        private final Long productId;


        private CreateProviderPaymentDetailRequestBuilder createProviderPaymentDetailRequestBuilder;

        public CreateOtherProductProviderBookingBuilder(Status status, BigDecimal amount, String currency, Long productId) {
            this.status = status;
            this.amount = amount;
            this.currency = currency;
            this.productId = productId;
        }

        public CreateOtherProductProviderBookingBuilder createProviderPaymentDetailRequestBuilder(CreateProviderPaymentDetailRequestBuilder createProviderPaymentDetailRequestBuilder) {
            this.createProviderPaymentDetailRequestBuilder = createProviderPaymentDetailRequestBuilder;
            return this;
        }

        CreateProviderPaymentDetailRequestBuilder getCreateProviderPaymentDetailRequestBuilder() {
            return createProviderPaymentDetailRequestBuilder;
        }

        public CreateOtherProductProviderBooking build() {
            CreateOtherProductProviderBooking createProviderBooking = new CreateOtherProductProviderBooking();
            createProviderBooking.setStatus(status);
            Money money = new Money();
            money.setAmount(amount);
            money.setCurrency(currency);
            createProviderBooking.setTotalProviderPrice(money);
            createProviderBooking.setProductId(productId);
            if (nonNull(getCreateProviderPaymentDetailRequestBuilder())) {
                createProviderBooking.setCreateProviderPaymentDetailRequests(Collections.singletonList(getCreateProviderPaymentDetailRequestBuilder().build()));
            }
            return createProviderBooking;
        }
    }
}
