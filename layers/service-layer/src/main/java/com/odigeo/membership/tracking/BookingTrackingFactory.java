package com.odigeo.membership.tracking;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.Membership;
import com.odigeo.membership.booking.BookingDetailWrapper;
import com.odigeo.membership.member.MembershipValidationService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class BookingTrackingFactory {

    public AbstractBookingTracking create(BookingDetailWrapper bookingDetailWrapper, Membership membership) {
        return applyNewVatModel(membership)
            ? new BookingTrackingNewVatModel(membership, bookingDetailWrapper)
            : new BookingTrackingOldModel(membership, bookingDetailWrapper);
    }

    boolean applyNewVatModel(Membership membership) {
        LocalDateTime activationDate = membership.getActivationDate();
        if (Objects.nonNull(activationDate)) {
            return LocalDate.parse(getMembershipValidationService().getNewVatModelDate())
                .isBefore(activationDate.toLocalDate());
        }
        return false;
    }

    private MembershipValidationService getMembershipValidationService() {
        return ConfigurationEngine.getInstance(MembershipValidationService.class);
    }
}
