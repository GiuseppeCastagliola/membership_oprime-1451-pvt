package com.odigeo.membership.tracking;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.AutorenewalTracking;
import com.odigeo.membership.member.AbstractServiceBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.sql.SQLException;

@Stateless
@Local(AutorenewalTrackingService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AutorenewalTrackingServiceBean extends AbstractServiceBean implements AutorenewalTrackingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AutorenewalTrackingServiceBean.class);

    @Override
    public void trackAutorenewalOperation(AutorenewalTracking autorenewalTracking) {
        try {
            getMembershipStore().insertAutorenewalTracking(dataSource, autorenewalTracking);
        } catch (SQLException e) {
            LOGGER.error("Error inserting autorenewal tracking", e);
        }
    }

    AutorenewalTrackingStore getMembershipStore() {
        return ConfigurationEngine.getInstance(AutorenewalTrackingStore.class);
    }
}
