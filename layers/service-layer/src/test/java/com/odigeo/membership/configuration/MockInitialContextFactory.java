package com.odigeo.membership.configuration;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Module;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import java.util.Hashtable;

import static org.mockito.Mockito.when;

public class MockInitialContextFactory implements InitialContextFactory {

    private static final ThreadLocal<Context> CURRENT_CONTEXT = new ThreadLocal<Context>();

    public static void testAvailableServiceFromConfigurationEngine(Module[] modules, String accommodationGateway, Class<?> type, Context context) throws NamingException {
        setCurrentContext(context);
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, MockInitialContextFactory.class.getName());
        when(context.lookup(JeeServiceLocator.JNDI_APP_NAME)).thenReturn(accommodationGateway);
        ConfigurationEngine.init(modules);
        ConfigurationEngine.getInstance(type);
        clearCurrentContext();
        System.clearProperty(Context.INITIAL_CONTEXT_FACTORY);
    }

    @Override
    public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
        return CURRENT_CONTEXT.get();
    }

    public static void setCurrentContext(Context context) {
        CURRENT_CONTEXT.set(context);
    }

    public static void clearCurrentContext() {
        CURRENT_CONTEXT.remove();
    }

}
