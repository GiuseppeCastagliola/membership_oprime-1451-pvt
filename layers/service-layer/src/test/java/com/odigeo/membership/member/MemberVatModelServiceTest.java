package com.odigeo.membership.member;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MemberVatModelServiceTest {

    private static final String TEST_CUT_OVER_DATE = "2018-12-16";
    private static final String LOCAL_DATE_TODAY = LocalDate.now().toString();
    private static final String LOCAL_DATE_TOMORROW = String.valueOf(LocalDate.now().plusDays(1L));
    private MemberVatModelService memberVatModelService;

    @BeforeMethod
    public void setUp() {
        this.memberVatModelService = assembleMemberVatModelService();
    }

    private MemberVatModelService assembleMemberVatModelService() {
        final MemberVatModelService memberVatModelService = new MemberVatModelService();
        memberVatModelService.setCutOverDate(TEST_CUT_OVER_DATE);
        return memberVatModelService;
    }

    @Test
    public void testIsCutOverDateExpiredReturnsFalseForEarlierDate() {
        assertFalse(this.memberVatModelService.isCutOverDateExpired());
    }

    @Test
    public void testIsCutOverDateExpiredReturnsFalseForSameDate() {
        this.memberVatModelService.setCutOverDate(LOCAL_DATE_TODAY);
        assertFalse(this.memberVatModelService.isCutOverDateExpired());
    }

    @Test
    public void testIsCutOverDateExpiredReturnsTrueForLaterDate() {
        this.memberVatModelService.setCutOverDate(LOCAL_DATE_TOMORROW);
        assertTrue(this.memberVatModelService.isCutOverDateExpired());
    }
}
