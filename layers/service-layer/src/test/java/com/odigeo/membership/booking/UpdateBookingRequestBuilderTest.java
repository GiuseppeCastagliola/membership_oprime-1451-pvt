package com.odigeo.membership.booking;

import com.odigeo.bookingapi.v12.requests.FeeRequest;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import org.testng.annotations.Test;

import java.util.Collections;

import static junit.framework.Assert.assertNull;
import static org.testng.Assert.assertEquals;

public class UpdateBookingRequestBuilderTest {

    @Test
    public void testGetFees() {
        FeeRequest feeRequest = new FeeRequest();
        UpdateBookingRequest updateBookingRequest = new UpdateBookingRequestBuilder().setFeeRequests(feeRequest).build();
        assertEquals(updateBookingRequest.getBookingFees(), Collections.singletonList(feeRequest));
    }

    @Test
    public void testGetNullFees() {
        UpdateBookingRequest updateBookingRequest = new UpdateBookingRequestBuilder().build();
        assertNull(updateBookingRequest.getBookingFees());
    }
}