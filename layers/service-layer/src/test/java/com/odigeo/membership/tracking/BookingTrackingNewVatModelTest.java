package com.odigeo.membership.tracking;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.bookingapi.BookingApiManager;
import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.FeeBuilder;
import com.odigeo.bookingapi.mock.v12.response.MembershipPerksBuilder;
import com.odigeo.bookingapi.mock.v12.response.WebsiteBuilder;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.Fee;
import com.odigeo.bookingapi.v12.responses.FeeContainer;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.booking.BookingDetailWrapper;
import com.odigeo.membership.booking.MembershipVatModelFee;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.member.MembershipStore;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.Random;

import static com.odigeo.membership.booking.MembershipVatModelFee.AVOID_FEES;
import static com.odigeo.membership.booking.MembershipVatModelFee.COST_FEE;
import static com.odigeo.membership.booking.MembershipVatModelFee.PERKS;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BookingTrackingNewVatModelTest {

    private static final Random RANDOM = new Random();
    private static final String SITE_ES = "ES";
    private static final String BRAND_ED = "ED";
    private static final String CONTRACT = "CONTRACT";
    private static final long FEE_CONTAINER_ID = 999L;
    private static final long MEMBERSHIP_ID = 101L;
    private static final long BOOKING_ID = 123L;
    private static final BigDecimal BALANCE_BIGGER_THAN_AVOID = new BigDecimal(15);
    private static final BigDecimal BALANCE_LESS_THAN_AVOID = new BigDecimal(5);
    private static final BigDecimal AVOID_FEE_AMOUNT = new BigDecimal(-10);
    private static final BigDecimal COST_FEE_AMOUNT = new BigDecimal(0);
    private static final BigDecimal PERKS_AMOUNT = new BigDecimal(-20);
    private static final BigDecimal CHANGED_AVOID_AMOUNT = new BigDecimal(-15);

    @Mock
    private DataSource dataSource;
    @Mock
    private Connection connection;
    @Mock
    private BookingApiManager bookingApiManager;
    @Mock
    private BookingTrackingStore bookingTrackingStore;
    @Mock
    private MembershipStore membershipStore;

    private Membership membership;
    private AbstractBookingTracking bookingTracking;
    private BookingDetail bookingDetail;
    private BookingDetailWrapper bookingDetailWrapper;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        membership = new MembershipBuilder().setId(MEMBERSHIP_ID).build();
        bookingDetail = buildBookingDetail();
        bookingDetailWrapper = new BookingDetailWrapper(bookingDetail);
        bookingTracking = new BookingTrackingNewVatModel(membership, bookingDetailWrapper);
        ConfigurationEngine.init(this::configure);
        when(dataSource.getConnection()).thenReturn(connection);
    }

    private void configure(Binder binder) {
        binder.bind(BookingTrackingStore.class).toInstance(bookingTrackingStore);
        binder.bind(BookingApiManager.class).toInstance(bookingApiManager);
        binder.bind(MembershipStore.class).toInstance(membershipStore);
    }

    @Test
    public void testGetBookingTrackingBalancedFees() throws RetryableException, BuilderException {
        // Given
        setUpMemberBalanceAndBookingFees(BALANCE_BIGGER_THAN_AVOID);

        // When
        Optional<BookingTracking> optionalBookingTracking = this.bookingTracking.getBookingTracking();

        // Then
        assertTrue(optionalBookingTracking.isPresent());
        BookingTracking bookingTracking = optionalBookingTracking.get();
        assertEquals(bookingTracking.getMembershipId(), MEMBERSHIP_ID);
        assertEquals(bookingTracking.getBookingId(), bookingDetail.getBookingBasicInfo().getId());
        assertEquals(bookingTracking.getAvoidFeeAmount(), AVOID_FEE_AMOUNT);
        assertEquals(bookingTracking.getCostFeeAmount(), COST_FEE_AMOUNT);
        assertEquals(bookingTracking.getPerksAmount(), PERKS_AMOUNT);
    }

    @Test
    public void testGetBookingTrackingUnbalancedFees() throws RetryableException, BuilderException, DataAccessException {
        // Given
        setUpMemberBalanceAndBookingFees(BALANCE_LESS_THAN_AVOID);
        when(bookingApiManager.updateBooking(anyLong(), any(UpdateBookingRequest.class))).thenReturn(bookingDetail);

        // When
        Optional<BookingTracking> optionalBookingTracking = this.bookingTracking.getBookingTracking();

        // Then
        assertFalse(optionalBookingTracking.isPresent());
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testGetBookingTrackingNullResponseBookingApi() throws RetryableException, BuilderException,
        DataAccessException {
        // Given
        setUpMemberBalanceAndBookingFees(BALANCE_LESS_THAN_AVOID);
        when(bookingApiManager.updateBooking(anyLong(), any(UpdateBookingRequest.class))).thenReturn(null);

        // When
        this.bookingTracking.getBookingTracking();
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testGetBookingTrackingDataAccessExceptionBookingApi() throws RetryableException, BuilderException,
        DataAccessException {
        // Given
        setUpMemberBalanceAndBookingFees(BALANCE_LESS_THAN_AVOID);
        when(bookingApiManager.updateBooking(anyLong(), any(UpdateBookingRequest.class)))
            .thenThrow(new DataAccessException(StringUtils.EMPTY));

        // When
        this.bookingTracking.getBookingTracking();
    }

    @Test
    public void testDeleteTrackedBookingAndRestoreBalance() throws SQLException, RetryableException, BuilderException {
        // Given
        setUpMemberBalanceAndBookingFees(BALANCE_BIGGER_THAN_AVOID);
        BookingTracking alreadyTrackedBooking = getAlreadyTrackedBooking(AVOID_FEE_AMOUNT, COST_FEE_AMOUNT, PERKS_AMOUNT);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(alreadyTrackedBooking);
        BigDecimal memberRestoredBalance = BALANCE_BIGGER_THAN_AVOID.subtract(AVOID_FEE_AMOUNT);

        // When
        bookingTracking.deleteTrackedBookingAndRestoreBalance(dataSource);

        // Then
        assertEquals(membership.getBalance(), memberRestoredBalance);
        verify(membershipStore).updateMembershipBalance(eq(dataSource), eq(MEMBERSHIP_ID), eq(memberRestoredBalance));
        verify(bookingTrackingStore).removeTrackedBooking(eq(dataSource), eq(BOOKING_ID));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testDeleteTrackedBookingAndRestoreBalanceSQLException() throws SQLException, RetryableException {
        // Given
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenThrow(new SQLException());

        // When
        bookingTracking.deleteTrackedBookingAndRestoreBalance(dataSource);
    }

    @Test
    public void testAreFeesAmountChangedSameFees() throws BuilderException {
        // Given
        BookingTracking alreadyTrackedBooking = getAlreadyTrackedBooking(AVOID_FEE_AMOUNT, COST_FEE_AMOUNT, PERKS_AMOUNT);
        setUpBookingMembershipFees(AVOID_FEE_AMOUNT, COST_FEE_AMOUNT, PERKS_AMOUNT);

        // When
        boolean feesAmountChanged = bookingTracking.areFeesAmountChanged(alreadyTrackedBooking);

        // Then
        assertFalse(feesAmountChanged);
    }

    @Test
    public void testAreFeesAmountChangedChangedFees() throws BuilderException {
        // Given
        BookingTracking alreadyTrackedBooking = getAlreadyTrackedBooking(AVOID_FEE_AMOUNT, COST_FEE_AMOUNT, PERKS_AMOUNT);
        setUpBookingMembershipFees(CHANGED_AVOID_AMOUNT, COST_FEE_AMOUNT, PERKS_AMOUNT);

        // When
        boolean feesAmountChanged = bookingTracking.areFeesAmountChanged(alreadyTrackedBooking);

        // Then
        assertTrue(feesAmountChanged);
    }

    @Test
    public void testFeesAreUnbalanced() {
        // Given
        BookingTrackingNewVatModel newVatModelTracking = (BookingTrackingNewVatModel) this.bookingTracking;

        // Then
        assertFalse(newVatModelTracking.feesAreUnbalanced(BALANCE_BIGGER_THAN_AVOID, COST_FEE_AMOUNT, AVOID_FEE_AMOUNT));
        assertTrue(newVatModelTracking.feesAreUnbalanced(BALANCE_LESS_THAN_AVOID, COST_FEE_AMOUNT, AVOID_FEE_AMOUNT));
    }

    private void setUpMemberBalanceAndBookingFees(BigDecimal balance) throws BuilderException {
        membership.setBalance(balance);
        setUpBookingMembershipFees(AVOID_FEE_AMOUNT, COST_FEE_AMOUNT, PERKS_AMOUNT);
    }

    private void setUpBookingMembershipFees(BigDecimal avoidAmount, BigDecimal costAmount, BigDecimal perks) throws BuilderException {
        bookingDetail.setAllFeeContainers(Arrays.asList(
            buildMembershipFeeContainer(buildFee(AVOID_FEES, avoidAmount)),
            buildMembershipFeeContainer(buildFee(COST_FEE, costAmount)),
            buildMembershipFeeContainer(buildFee(PERKS, perks))
        ));
    }

    private Fee buildFee(MembershipVatModelFee fee, BigDecimal amount) throws BuilderException {
        return new FeeBuilder().amount(amount).subCode(fee.getFeeCode()).build(RANDOM);
    }

    private FeeContainer buildMembershipFeeContainer(Fee... fees) {
        FeeContainer feeContainer = new FeeContainer();
        feeContainer.setId(FEE_CONTAINER_ID);
        feeContainer.setFees(Arrays.asList(fees.clone()));
        return feeContainer;
    }

    private BookingTracking getAlreadyTrackedBooking(BigDecimal avoidAmount, BigDecimal costAmount, BigDecimal perksAmount) {
        BookingTracking alreadyTrackedBooking = new BookingTracking();
        alreadyTrackedBooking.setAvoidFeeAmount(avoidAmount);
        alreadyTrackedBooking.setPerksAmount(perksAmount);
        alreadyTrackedBooking.setCostFeeAmount(costAmount);
        return alreadyTrackedBooking;
    }

    private BookingDetail buildBookingDetail() throws BuilderException {
        Calendar bookingCalendar = Calendar.getInstance();
        bookingCalendar.setTime(new Date());
        return new BookingDetailBuilder()
            .bookingBasicInfoBuilder(new BookingBasicInfoBuilder()
                .id(BOOKING_ID).creationDate(bookingCalendar)
                .website(new WebsiteBuilder().brand(BRAND_ED).code(SITE_ES)))
            .bookingStatus(CONTRACT)
            .membershipPerks(new MembershipPerksBuilder().memberId(MEMBERSHIP_ID).feeContainerId(FEE_CONTAINER_ID))
            .build(RANDOM);
    }
}