package com.odigeo.membership.tracking;

import com.odigeo.membership.AutorenewalTracking;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.assertSame;

public class AutorenewalTrackingServiceBeanTest {
    private AutorenewalTrackingServiceBean autorenewalTrackingServiceBean;

    @Mock
    AutorenewalTrackingStore autorenewalTrackingStore;

    @Captor
    ArgumentCaptor<AutorenewalTracking> autorenewalTrackingBuilderCaptor;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        autorenewalTrackingServiceBean = spy(new AutorenewalTrackingServiceBean());
        doReturn(autorenewalTrackingStore).when(autorenewalTrackingServiceBean).getMembershipStore();
    }

    @Test
    public void testTrackAutorenewalOperation() throws SQLException {
        AutorenewalTracking autorenewalTracking = new AutorenewalTracking();
        autorenewalTrackingServiceBean.trackAutorenewalOperation(autorenewalTracking);
        verify(autorenewalTrackingStore).insertAutorenewalTracking(any(), autorenewalTrackingBuilderCaptor.capture());
        assertSame(autorenewalTrackingBuilderCaptor.getValue(), autorenewalTracking);
    }

    @Test
    public void testExceptionDuringTrackAutorenewalOperation() throws SQLException {
        doThrow(new SQLException()).when(autorenewalTrackingStore).insertAutorenewalTracking(any(), any());
        AutorenewalTracking autorenewalTracking = new AutorenewalTracking();
        autorenewalTrackingServiceBean.trackAutorenewalOperation(autorenewalTracking);
        verify(autorenewalTrackingStore).insertAutorenewalTracking(any(), autorenewalTrackingBuilderCaptor.capture());
        assertSame(autorenewalTrackingBuilderCaptor.getValue(), autorenewalTracking);
    }
}