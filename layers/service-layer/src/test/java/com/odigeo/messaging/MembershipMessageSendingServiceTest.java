package com.odigeo.messaging;

import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.messaging.kafka.MembershipKafkaSender;
import com.odigeo.messaging.utils.MessageDataAccessException;
import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;

public class MembershipMessageSendingServiceTest {

    private static final long MEMBERSHIP_ID = 123L;

    @Mock
    private Appender mockAppender;
    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;
    @Mock
    private MembershipKafkaSender membershipKafkaSenderMock;
    @Mock
    private MembershipSubscriptionMessageService membershipSubscriptionMessageServiceMock;

    private MembershipMessageSendingService membershipMessageSendingService;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        Logger.getRootLogger().addAppender(mockAppender);
        membershipMessageSendingService = new MembershipMessageSendingService(membershipKafkaSenderMock, membershipSubscriptionMessageServiceMock);
    }

    @Test
    public void testSendMembershipIdToMembershipReporter() throws MessageDataAccessException {
        membershipMessageSendingService.sendMembershipIdToMembershipReporter(MEMBERSHIP_ID);
        ArgumentCaptor<MembershipUpdateMessage> membershipUpdateMessageCaptor = ArgumentCaptor.forClass(MembershipUpdateMessage.class);
        verify(membershipKafkaSenderMock).sendMembershipUpdateMessageToKafka(membershipUpdateMessageCaptor.capture());
        assertEquals(String.valueOf(MEMBERSHIP_ID), membershipUpdateMessageCaptor.getValue().getMembershipId());
    }

    @Test (expectedExceptionsMessageRegExp = "Error sending ID [0-9]+ to Kafka topic" )
    public void testSendMembershipIdToMembershipReporterFail() throws MessageDataAccessException {
        doThrow(new MessageDataAccessException("", null)).when(membershipKafkaSenderMock).sendMembershipUpdateMessageToKafka(any());
        membershipMessageSendingService.sendMembershipIdToMembershipReporter(MEMBERSHIP_ID);
    }

    @Test
    public void testSendMembershipSubscriptionMessage() throws MessageDataAccessException {
        MembershipSubscriptionMessage testMessage = Mockito.mock(MembershipSubscriptionMessage.class);
        MembershipSubscriptionMessage expectedMessage = Mockito.mock(MembershipSubscriptionMessage.class);
        when(membershipSubscriptionMessageServiceMock.completeSubscriptionMessage(testMessage)).thenReturn(expectedMessage);
        assertTrue(membershipMessageSendingService.sendMembershipSubscriptionMessage(testMessage));
        verify(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(expectedMessage);
    }

    @Test
    public void testSendMembershipSubscriptionMessageExceptionHandled() throws MessageDataAccessException {
        final String errorMessage = "Error - Membership subscription Message to Kafka topic for email";

        doThrow(new MessageDataAccessException("", null)).when(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(any(MembershipSubscriptionMessage.class));

        final MembershipSubscriptionMessage membershipSubscriptionMessage = Mockito.mock(MembershipSubscriptionMessage.class);
        when(membershipSubscriptionMessageServiceMock.completeSubscriptionMessage(any(MembershipSubscriptionMessage.class))).thenReturn(membershipSubscriptionMessage);

        assertFalse(membershipMessageSendingService.sendMembershipSubscriptionMessage(membershipSubscriptionMessage));
        verify(mockAppender, atLeastOnce()).doAppend(captorLoggingEvent.capture());
        assertTrue(captorLoggingEvent.getAllValues()
                .stream()
                .map(LoggingEvent::getMessage)
                .map(Object::toString)
                .anyMatch(message -> message.startsWith(errorMessage))
        );
    }

    @Test
    public void testSendMembershipMailerMessage() {
        //GIVEN
        MembershipMailerMessage mockMessage = mock(MembershipMailerMessage.class);
        //WHEN
        membershipMessageSendingService.sendMembershipMailerMessage(mockMessage, false);
        //THEN
        verify(membershipKafkaSenderMock).sendMembershipMailerMessageToKafka(mockMessage, false);
    }
}