package com.odigeo.membership.member.rest.exception.mapper;

import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.member.rest.utils.BaseExceptionBean;
import com.odigeo.membership.member.rest.utils.MembershipServiceExceptionBean;

import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by roc.arajol on 01-Feb-18.
 */
@Provider
public class MembershipServiceExceptionMapper extends BaseExceptionMapper<MembershipServiceException> implements ExceptionMapper<MembershipServiceException> {

    @Override
    protected BaseExceptionBean beanToSend(MembershipServiceException exception) {
        return new MembershipServiceExceptionBean(exception, true);
    }
}
