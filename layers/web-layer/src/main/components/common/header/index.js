import React, { Component } from 'react';
import Bem from "react-bem-helper";
import { Col, Layout, Row } from "antd";
import './style.scss';

const { Header: AntHeader } = Layout;
const classes = new Bem('header');

class Header extends Component {
    render() {
        return (
            <AntHeader {...classes('')}>
                <Row type="flex" justify="center">
                    <Col>
                        <h1>{this.props.text}</h1>
                    </Col>
                </Row>
            </AntHeader>
        )
    }
}

export default Header
