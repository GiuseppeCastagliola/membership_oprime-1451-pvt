package com.odigeo.membership.member.memberapi.v1.util;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.exception.ActivatedMembershipException;
import com.odigeo.membership.exception.MembershipForbiddenException;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.exception.MembershipNotFoundException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class MembershipExceptionMapperTest {

    private static final String EXCEPTION_MSG = "TEST";

    @DataProvider(name = "unmappedErrors")
    static Object[][] unmappedErrors() {
        return new Object[][]{
                new Object[]{new DataAccessException(EXCEPTION_MSG)},
                new Object[]{new BookingApiException(EXCEPTION_MSG)},
                new Object[]{new IOException(EXCEPTION_MSG)}
        };
    }

    @Test (dataProvider = "unmappedErrors", expectedExceptions = MembershipInternalServerErrorException.class)
    public void getMembershipApiExceptionDataAccessException(Exception exception) {
        //When
        throw MembershipExceptionMapper.map(exception);
    }

    @Test (expectedExceptions = MembershipInternalServerErrorException.class)
    public void getMembershipApiExceptionBookingApiException() {
        //When
        throw MembershipExceptionMapper.map(new BookingApiException(EXCEPTION_MSG));
    }

    @Test (expectedExceptions = MembershipNotFoundException.class)
    public void getMembershipApiExceptionMissingElementException() {
        //When
        throw  MembershipExceptionMapper.map(new MissingElementException(EXCEPTION_MSG));
    }

    @Test (expectedExceptions = MembershipForbiddenException.class)
    public void getMembershipApiExceptionMembershipForbiddenException() {
        //When
        throw  MembershipExceptionMapper.map(new ActivatedMembershipException(EXCEPTION_MSG));
    }
}