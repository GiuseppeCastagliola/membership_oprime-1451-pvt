package com.odigeo.product.membership.service;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipFee;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipProductService;
import com.odigeo.membership.member.MembershipValidationService;
import com.odigeo.membership.member.UpdateMembershipService;
import com.odigeo.product.v2.exception.ProductException;
import com.odigeo.product.v2.model.Fee;
import com.odigeo.product.v2.model.Product;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class ProductServiceV2NewTest {

    private static final String PRODUCT_ID = "123";
    private static final String WEBSITE = "ES";
    private static final String TRANSACTION_PRODUCT_ID = "456";
    private static final String CURRENCY = "EUR";
    private static final String MEMBERSHIP_ID = "321";
    private static final Long MEMBER_ACCOUNT_ID = 111L;
    private static final LocalDateTime NOW = LocalDateTime.now();
    private static final LocalDateTime NEXT_YEAR = NOW.plusYears(1);
    private static final String RENEWAL_FEE_SUBCODE = "AE10";
    private static final String DEFAULT_EXCEPTION = "Default Exception";
    private static final DataAccessException DATA_ACCESS_EXCEPTION = new DataAccessException(DEFAULT_EXCEPTION);
    private static final MissingElementException MISSING_ELEMENT_EXCEPTION = new MissingElementException(DEFAULT_EXCEPTION);

    private static final Membership MEMBERSHIP = new MembershipBuilder().setId(Long.parseLong(MEMBERSHIP_ID)).setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(NOW).setExpirationDate(NEXT_YEAR).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(ProductStatus.INIT).build();
    private static final Membership MEMBERSHIP_CONTRACT = new MembershipBuilder().setId(Long.parseLong(MEMBERSHIP_ID)).setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(NOW).setExpirationDate(NEXT_YEAR).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(ProductStatus.CONTRACT).build();
    private static final MembershipFee MEMBERSHIP_FEE = new MembershipFee(MEMBERSHIP_ID, BigDecimal.TEN, CURRENCY, "RENEWAL_FEE");

    private ProductServiceV2New productService;

    @Mock
    private MemberService memberService;
    @Mock
    private MembershipProductService membershipProductService;
    @Mock
    private MembershipValidationService membershipValidationService;
    @Mock
    private UpdateMembershipService updateMembershipService;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        productService = new ProductServiceV2New(memberService, membershipProductService,
                membershipValidationService, updateMembershipService);
    }

    @Test
    public void testGetProductHappyPathWithSubCode() throws ProductException, MissingElementException, DataAccessException {
        when(memberService.getMembershipById(Long.parseLong(PRODUCT_ID))).thenReturn(MEMBERSHIP);
        when(membershipProductService.getMembershipFees(Long.parseLong(PRODUCT_ID))).thenReturn(Collections.singletonList(MEMBERSHIP_FEE));
        when(membershipValidationService.getNewVatModelDate()).thenReturn(LocalDate.now().minusDays(1).format(DateTimeFormatter.ISO_LOCAL_DATE));
        final Product product = productService.getProduct(PRODUCT_ID);
        assertNotNull(product);
        final List<Fee> fees = product.getFees();
        assertNotNull(fees);
        assertFalse(fees.isEmpty());
        final Fee fee = fees.get(0);
        assertNotNull(fee);
        assertEquals(fee.getSubCode(), RENEWAL_FEE_SUBCODE);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testGetProductWhenNotFound() throws ProductException {
        productService.getProduct(null);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testGetProductDataAccessException() throws ProductException, MissingElementException, DataAccessException {
        mockGetMemberByIdToThrowException(new DataAccessException(DEFAULT_EXCEPTION));
        productService.getProduct(PRODUCT_ID);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testGetProductMissingElementException() throws ProductException, MissingElementException, DataAccessException {
        mockGetMemberByIdToThrowException(MISSING_ELEMENT_EXCEPTION);
        productService.getProduct(PRODUCT_ID);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testSaveCompleteOrderMembershipNotFound() throws ProductException, DataAccessException, MissingElementException {
        mockGetMemberByIdToThrowException(new MissingElementException(DEFAULT_EXCEPTION));
        productService.saveProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID, false);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testSaveCompleteOrderMembershipDataAccessException() throws ProductException, DataAccessException, MissingElementException {
        mockGetMemberByIdToThrowException(DATA_ACCESS_EXCEPTION);
        productService.saveProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID, false);
    }

    private void mockGetMemberByIdToThrowException(Exception exception) throws MissingElementException, DataAccessException {
        when(memberService.getMembershipById(Long.parseLong(PRODUCT_ID))).thenThrow(exception);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testSaveCompleteOrderNotInContract() throws ProductException, DataAccessException, MissingElementException {
        when(memberService.getMembershipById(Long.parseLong(PRODUCT_ID))).thenReturn(MEMBERSHIP);
        productService.saveProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID, false);
    }

    @Test
    public void testSaveCompleteOrderInContract() throws ProductException, DataAccessException, MissingElementException {
        when(memberService.getMembershipById(Long.parseLong(PRODUCT_ID))).thenReturn(MEMBERSHIP_CONTRACT);
        productService.saveProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID, false);
    }

    @Test
    public void testCommitMembership() throws DataAccessException {
        when(updateMembershipService.activatePendingToCollect(MEMBERSHIP)).thenReturn(Boolean.TRUE);
        assertTrue(productService.commitMembership(MEMBERSHIP));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testCommitMembershipFailure() throws DataAccessException {
        when(updateMembershipService.activatePendingToCollect(MEMBERSHIP))
                .thenThrow(new DataAccessException(DEFAULT_EXCEPTION));
        productService.commitMembership(MEMBERSHIP);
    }
}
